using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSoundController : MonoBehaviour
{
    public AudioClip SonidoMuerteZombie;
    public AudioClip SonidoZombieActivado;
     EnemyLifeController enemyLifeController;
    AudioSource AudioSourceZombie;
    void Start()
    {
        enemyLifeController = gameObject.GetComponent<EnemyLifeController>();
        AudioSourceZombie = gameObject.GetComponent<AudioSource>();
    }

    private void Update()
    { 
        if (enemyLifeController.life <0 && enemyLifeController.life == 0)
        {
            AudioSourceZombie.Stop();
            AudioSourceZombie.clip = SonidoMuerteZombie;
            AudioSourceZombie.Play();
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            AudioSourceZombie.Stop();
            AudioSourceZombie.clip = SonidoZombieActivado;
            AudioSourceZombie.loop = enabled;
            AudioSourceZombie.Play();
        }
    }
}
