using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class CheckpointController : MonoBehaviour
{
    public bool firstTime;

    public AudioClip mysound;
    public AudioClip endsound;
    public AudioClip staysound;
    public AudioSource source1;

    public GameObject savingUI;

    // Start is called before the first frame update
    void Start()
    {
        source1 = transform.GetChild(2).gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SaveState(/*params per a guardar*/List<bool> levelspassed,int kills)
    {
        //ingamesaving:
        levelspassed[levelspassed.FindLastIndex(x => true) + 1] = true;
        //WriteExternalfile:
        if (!File.Exists(GameManager.Instance.pathSave)) File.Create(GameManager.Instance.pathSave).Close();
        File.WriteAllText(GameManager.Instance.pathSave,$"{DateTime.Now}/{JsonUtility.ToJson(levelspassed)}/{kills}");
    }


    public Vector3 checkpointPosition;
    public Quaternion checkpointRotation;
    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Player")&&!firstTime)
        {
            savingUI.SetActive(true);
            //Agafa la posici� del Player en el checkPoint
            checkpointPosition = other.transform.position;
            checkpointRotation = other.transform.rotation;

            other.gameObject.GetComponent<PlayerLifeController>().ModifyLife(-100);
            GetComponent<Animator>().SetBool("enter1st",true);
            //SaveState(GameManager.Instance.checkpointspassed,GameManager.Instance.kills);
            
        }
        else if(other.CompareTag("Player")) { GetComponent<Animator>().SetBool("enter1st", true); }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GetComponent<Animator>().SetBool("playerout", true);
            savingUI.SetActive(false);
        }
    }

    public void SoundPlay(float track)
    {
        switch (track)
        {
            case 1:
                source1.loop = false;
                source1.clip = mysound;
                source1.Play();
                break;
            case 2:
                source1.Stop();
                source1.loop = false;
                source1.clip = endsound;
                source1.Play();
                break;
            case 3:
                
                source1.loop = true;
                source1.clip = staysound;
                source1.Play();
                break;
        }
    }
}
