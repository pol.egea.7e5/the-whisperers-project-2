using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBehaivour : MonoBehaviour
{
    private GameObject _escenari;
    // Start is called before the first frame update
    void Start()
    {
        _escenari = transform.parent.gameObject;
    }

    void DoorTraspaset()
    {
        _escenari.GetComponent<RoomController>().EnterRoom();
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log(other.name);
        DoorTraspaset();
    }
}
