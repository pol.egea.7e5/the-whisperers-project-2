using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomGenerator : MonoBehaviour
{
    public GameObject player;

    public List<GameObject> Levels = new List<GameObject>();
    //void Start()
    //{
    //    var position = Vector3.zero;
        
    //    foreach (var levelVariant in Levels)
    //    {
    //        Debug.Log($"Level : {levelVariant} with pos = {levelVariant.transform.position}");
    //        GameObject prefabVariant = Instantiate(levelVariant, position, Quaternion.identity);
    //        position = prefabVariant.GetComponent<RoomPositions>().exit.position + prefabVariant.GetComponent<RoomPositions>().start.position;
    //    }
    //}

    void Start()
    {
        var position = Vector3.zero;
        Shuffle(Levels);

        for (int i = 0; i < Levels.Count; i++)
        {
            GameObject ActualprefabVariant = Instantiate(Levels[i], position, Quaternion.identity);
            if(i >= 1)
            {
                Debug.Log($"Level : {ActualprefabVariant} with pos = {ActualprefabVariant.transform.position}");
                RoomPositions NextRoomPositions = ActualprefabVariant.GetComponent<RoomPositions>();
                Debug.Log($"New Center Point {NextRoomPositions.exit.position - NextRoomPositions.start.position}");
                position += NextRoomPositions.exit.position - NextRoomPositions.start.position;
                ActualprefabVariant.transform.position = position;
            }
            
        }

    }

    void Shuffle<T>(List<T> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            int randomIndex = UnityEngine.Random.Range(i, list.Count);
            T temp = list[i];
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
    }

}