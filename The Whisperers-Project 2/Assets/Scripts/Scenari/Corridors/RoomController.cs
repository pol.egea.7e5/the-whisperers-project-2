using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnumLibrary;

public class RoomController : MonoBehaviour
{
    [SerializeField]
    private CorridorDirection _direction;
    private int _roomId;
    public CorridorDirection Direction
    {
        get { return _direction; }
    }
    public delegate void EnteringRoom(RoomController RC);
    public event EnteringRoom OnEnterRoom;
    public int RoomId
    {
        get {return _roomId;}
        set {_roomId = value;}
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
   public void EnterRoom()
    {
        OnEnterRoom(this);
    }
}
