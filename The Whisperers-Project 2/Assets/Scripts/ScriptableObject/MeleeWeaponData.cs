using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MeleeWeaponData", menuName = "MeleeWeaponData")]
public class MeleeWeaponData : WeaponData
{
    public float Damage;
}
