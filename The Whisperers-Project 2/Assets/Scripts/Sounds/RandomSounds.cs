using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSounds : MonoBehaviour
{
    public AudioClip Voces;
    public AudioClip Grito;
    public AudioClip SonidoRandom;
    AudioSource AudioSource;

    private void Start()
    {
        AudioSource= GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            int randomnumber = Random.Range(0, 2);
            Debug.Log(randomnumber);
            Debug.Log("Random Sound ");
            switch (randomnumber)
            {
                case 0:
                    AudioSource.clip = Voces;
                    break;
                case 1:
                    AudioSource.clip = Grito;
                    break;
                case 2:
                    AudioSource.clip = SonidoRandom;
                    break;
            }
            AudioSource.Play();
        }
    }
}
