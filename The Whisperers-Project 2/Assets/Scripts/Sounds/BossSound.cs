using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSound : MonoBehaviour
{
    AudioSource SoundEnemyBoss;
    public AudioClip[] clips;
   


    private void Start()
    {
        SoundEnemyBoss = gameObject.GetComponent<AudioSource>();
        AsigancionAudios();
    }


    void AsigancionAudios()
    {
        int numerorandom = Random.Range(0, clips.Length-1);
        SoundEnemyBoss.Stop();
        SoundEnemyBoss.clip = clips[numerorandom+1];
        SoundEnemyBoss.Play();

        /*switch (numerorandom)
        {
            case 0:
                SoundEnemyBoss.Stop();
                SoundEnemyBoss.clip = clip1;
                SoundEnemyBoss.Play();
            
                break;
            case 1:
                SoundEnemyBoss.Stop();
                SoundEnemyBoss.clip = clip2;
                SoundEnemyBoss.Play();
                
                break;
            case 2:
                SoundEnemyBoss.Stop();
                SoundEnemyBoss.clip = clip3;
                SoundEnemyBoss.Play();
               
                break;
            case 3:
                SoundEnemyBoss.Stop();
                SoundEnemyBoss.clip = clip4;
                SoundEnemyBoss.Play();
                
                break;
            case 4:
                SoundEnemyBoss.Stop();
                SoundEnemyBoss.clip = clip5;
                SoundEnemyBoss.Play();
               
                break;

        }*/

        EsperaSegundos();
    }
    IEnumerator EsperaSegundos()
    {
        int randomtime = Random.Range(30, 40);
        yield return new WaitForSeconds(randomtime);
        AsigancionAudios();
    }

   

}
