using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnumLibrary;
public class WeaponSpawnerController : MonoBehaviour
{
    [SerializeField]
    private WeaponList weaponList;
    private List<WeaponData> weapons;
    private int _counter;
    private bool _searching;
    private void Start()
    {
        _searching = false;
        weapons = new List<WeaponData>();
        weaponList.CurrentImaginaryWeapon = null;
        _counter = 0;
        StressManager.Instance.OnStressLevelChange += DisponibleWeapons;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void DisponibleWeapons(LevelOfStrees LevelStrees)
    {
        weapons.Clear();
        foreach (var weapon in weaponList.ImaginaryWeapons)
        {
            if(LevelStrees>=weapon.Strees)
                weapons.Add(weapon);
        }
        if (!weapons.Contains(weaponList.CurrentImaginaryWeapon)&&weaponList.CurrentImaginaryWeapon!=null&&_searching)
        {
            SearchWeapon();
        }
    }

    public void SearchWeapon()
    {
        _searching = true;
        if (_counter >= weapons.Count)
            _counter = 0;
        weaponList.CurrentImaginaryWeapon = weapons[_counter];
        Debug.Log(weaponList.CurrentImaginaryWeapon.name);
        GameManager.Instance.SetWeaponInMenu(weaponList.CurrentImaginaryWeapon);
        _counter++;
    }

    public void ExitMenu()
    {
        _searching = false;
    }

    public WeaponData GiveActualWeapon()
    {
        _searching = false;
        GameManager.Instance.DestroyActualWeaponMenu();
        return weaponList.CurrentImaginaryWeapon;  
    }
}
