using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnumLibrary;

public class QuadrupedEnemyController : EnemyController
{
    [SerializeField]
    private EnemyAnimationActions action;
    private Animator _anim;
    [SerializeField]
    private float rotationSpeed;
    [SerializeField]
    private GameObject _hitPlayer;
    [SerializeField]
    private float _jumpForce;
    private bool _jumping;
    [SerializeField]
    private bool _isGrounded;
    private bool _jumpAction;
    protected override void Start()
    {
        base.Start();
        _anim = GetComponent<Animator>();
        _hitPlayer.SetActive(false);
        _jumping = false;
        _jumpAction = false;
        _eLC= GetComponent<EnemyLifeController>();
    }

    
    void Update()
    {
        switch (action)
        {
            case EnemyAnimationActions.Walk:
                PatrolMovement();
                break;
            case EnemyAnimationActions.Run:
                RunMovement(Target);
                NearPlayer();
                break;
            case EnemyAnimationActions.TurnBehind:
                LookBehind();
                break;
            case EnemyAnimationActions.Jump:
                Jump();
                break;
            case EnemyAnimationActions.Attack:
                Attack();
                NearPlayer();
                break;
        }
        _anim.SetFloat("Life", _eLC.life);
    }

    void Attack()
    {
        rb.velocity = Vector3.zero;
        _anim.SetBool("Attack", true);
        _hitPlayer.SetActive(true);
        if (_hitPlayer!=null)
        {
            if (_hitPlayer.GetComponent<HitPlayer>().Ihit)
            {
                Target.GetComponent<PlayerLifeController>().ModifyLife(5);
                _hitPlayer.GetComponent<HitPlayer>().Ihit = false;
            }
        }
    }

    void Jump()
    {
        if (_isGrounded && _jumpAction)
        {
            _jumpAction = false;
            StateTransitor(EnemyAnimationActions.Run);
        }
        else
        {
            _anim.SetBool("Jump", !_jumpAction);
            _jumping = true;
        }
    }

   public void JumpAction()
   {
        rb.constraints = RigidbodyConstraints.None;
        rb.constraints = RigidbodyConstraints.FreezeRotation;
        Vector3 targetPosition = Target.transform.position;
        transform.LookAt(targetPosition);
        targetPosition.y += 1f;
        Vector3 direction = (targetPosition - transform.position).normalized;
        rb.velocity = direction * _jumpForce;
   }


    public void EndAttack()
    {
        _hitPlayer.SetActive(false);
        _anim.SetBool("Attack", false);
        StateTransitor(EnemyAnimationActions.Run);
    }

    void NearPlayer()
    {
            if (Physics.Raycast(transform.position, Target.transform.position - transform.position, 3f, ~toIgnore))
            {
                StateTransitor(EnemyAnimationActions.Attack);
            }
            else if(!_jumping)
            {
                StateTransitor(EnemyAnimationActions.Run);
                if (Physics.Raycast(transform.position, Target.transform.position - transform.position, 7, ~toIgnore) && !Physics.Raycast(transform.position, Target.transform.position - transform.position, 4, ~toIgnore))
                {
                    var numRandom = Random.Range(1, 10);
                    if (numRandom == 2)
                    {
                        rb.velocity = Vector3.zero;
                        StateTransitor(EnemyAnimationActions.Jump);
                    }
                }
            }
    }



    public void StateTransitor(EnemyAnimationActions newAction)
    {
            action = newAction;
    }

    void LookBehind()
    {
        if (transform.rotation.y < 180)
        {
            transform.rotation = new Quaternion(transform.rotation.x, rotationSpeed*Time.deltaTime, transform.rotation.z, transform.rotation.w).normalized;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            _isGrounded = true;
        }

        if (collision.gameObject.CompareTag("Player")&&action==EnemyAnimationActions.Jump)
        {
            rb.constraints = RigidbodyConstraints.FreezePositionY;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground")&&action==EnemyAnimationActions.Jump)
        {
            _isGrounded = false;
            _jumpAction = true;
        }
        if (collision.gameObject.CompareTag("Player") && action == EnemyAnimationActions.Jump)
        {
            rb.constraints = RigidbodyConstraints.None;
            rb.constraints = RigidbodyConstraints.FreezeRotation;
        }
    }

    public override void GetHit()
    {
        _anim.SetLayerWeight(1,1);
        StartCoroutine(HitAnimationCorroutine(_anim.GetCurrentAnimatorClipInfo(1)[0].clip.averageDuration));
    }

    private IEnumerator HitAnimationCorroutine(float time)
    {
        yield return new WaitForSeconds(time);
        _anim.SetLayerWeight(1,0);
    }

    public override void Death()
    {
        gameObject.GetComponent<Collider>().enabled= false;
       StateTransitor(EnemyAnimationActions.Death);
        Destroy(gameObject, 5f);
    }


}
