using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSpider : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip outsideZoneSound;
    public AudioClip insideZoneSound;

    private bool isInsideZone = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // El jugador entr� en la zona del enemigo
            isInsideZone = true;
            PlayInsideZoneSound();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // El jugador sali� de la zona del enemigo
            isInsideZone = false;
            PlayOutsideZoneSound();
        }
    }

    private void PlayInsideZoneSound()
    {
        audioSource.clip = insideZoneSound;
        audioSource.Play();
    }

    private void PlayOutsideZoneSound()
    {
        audioSource.clip = outsideZoneSound;
        audioSource.Play();
    }
}
