using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLifeController : CharacterLifeController
{
   
    public float maxVida = 100;
    private EnemyController _enemyC;
    
    void Start()
    {
        life = maxVida;
        _enemyC= GetComponent<EnemyController>();
    }

    void Update()
    {

    }

    public override void ModifyLife(float modifier)
    {
        base.ModifyLife(modifier);
        if (life <= 0)
            Death();
        _enemyC.GetHit();
    }
    public override void Death()
    {
       _enemyC.Death();
    }
    private void OnTriggerEnter(Collider other)
    {

    }
}
