using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnumLibrary;

public class ZombieControler : EnemyController
{
    int hitType;
    int hitda�o;
    int deadType;
    public EnemyAnimationActions actions;
    private Animator _anim;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        deadType = Random.Range(1,3);
        gameObject.GetComponent<Animator>().SetInteger("dead",deadType);
        hitType= Random.Range(1, 4);
        gameObject.GetComponent<Animator>().SetInteger("attack", hitType);
        _anim= GetComponent<Animator>();
    }

    private void Update()
    {
        switch (actions)
        {
            case EnemyAnimationActions.Idle:
                actions = EnemyAnimationActions.Walk;
                break;
            case EnemyAnimationActions.Walk:
                PatrolMovement(180);
                break;
            case EnemyAnimationActions.Run:
                RunMovement(Target,180);
                transform.GetChild(transform.childCount-1).gameObject.GetComponent<ZombiePlayerDetector>().CheckDistance();
                break;
            case EnemyAnimationActions.Attack:
                RunMovement(Target,180);
                transform.GetChild(transform.childCount - 1).gameObject.GetComponent<ZombiePlayerDetector>().CheckDistance();
                break;
            case EnemyAnimationActions.Die:
                break;

        }
    }
    public void AttackAndChange()
    {
        AttackToPlayer();
        ChangehitTypeAnd();
    }
    public void ChangehitTypeAnd()//aplicar al final de la animaci�n de hit
    {
        hitType = Random.Range(1, 4);
       _anim.SetInteger("attack", hitType);
    }
    public void AttackToPlayer()
    {
       Target.GetComponent<PlayerLifeController>().ModifyLife(hitda�o);
    }
    public void ChangeActionsType(EnemyAnimationActions accio)
    {
        if(actions!=EnemyAnimationActions.Idle)
        actions = accio;
    }

    public override void GetHit()
    {
       if (actions != EnemyAnimationActions.Die && actions != EnemyAnimationActions.PostDie)
       {
          _anim.Play("Hit");
          if (_eLC.life <= 0)
          {
              _anim.SetBool("live", false);
              actions = EnemyAnimationActions.Die;
          }
      }
      else
      {
          if (_eLC.life <= 0) _anim.SetBool("PostDieLive", false);
      }
    }

    public override void Death()
    {
        Destroy(gameObject);
    }
}
