using System.Collections;
using System.Collections.Generic;
using EnumLibrary;
using UnityEngine;

public class ZombiePlayerDetector : MonoBehaviour
{
    //If a Target was detected but enemy can't folow him, he can stay alert.
    public bool _playerDetected = false;
    private bool _playerSeeBehind = false;
    [SerializeField]
    private LayerMask toIgnore;
    [SerializeField]
    private float timeWaitingPlayer;
    private ZombieControler _zEC;
    private void Start()
    {
        _zEC= transform.parent.GetComponent<ZombieControler>();
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !_playerDetected)
        {
            if (Physics.Raycast(new Ray(transform.parent.position, other.transform.position - transform.parent.position), out RaycastHit hit, 15, ~toIgnore))
            {
                Debug.Log(hit.collider.gameObject.name);
                if (hit.collider.transform.parent.gameObject == other.gameObject)
                {
                    if (Vector3.Dot(other.transform.position - transform.parent.position, transform.forward) < 0)
                    {
                        Debug.Log("I have to see behind me");
                        StartCoroutine(TimeWaitThePLayerToBeSeen(timeWaitingPlayer));
                    }
                    else
                    {

                        Debug.Log("Player detected in front of me");
                        _zEC.Target = other.gameObject;
                        _playerDetected = true;
                        transform.parent.gameObject.GetComponent<ZombieControler>().actions = EnemyAnimationActions.Run;
                        transform.parent.gameObject.GetComponent<Animator>().SetBool("find", _playerDetected);

                    }
                }
            }
            else Debug.Log("Descarta");

        }
    }
    public void CheckDistance()
    {
        if (Physics.Raycast(new Ray(transform.parent.position, _zEC.Target.transform.position - transform.parent.position), out RaycastHit hit, 15, ~toIgnore))
        {
            Debug.Log(hit.collider.gameObject.name);
                if (hit.distance <= 3)
                {
                    Debug.Log("Enemy to hit");
                    transform.parent.gameObject.GetComponent<ZombieControler>().actions = EnemyAnimationActions.Attack;
                    transform.parent.gameObject.GetComponent<Animator>().SetBool("near", true);
                }
                else
                {
                    transform.parent.gameObject.GetComponent<Animator>().SetBool("near", false);
                }
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (_playerDetected)
        {
            Debug.Log("PlayerLost");
            _playerDetected = false;
            _playerSeeBehind = false;
            transform.parent.gameObject.GetComponent<ZombieControler>().actions = EnemyAnimationActions.Idle;
            transform.parent.gameObject.GetComponent<Animator>().SetBool("find", _playerDetected);
        }
    }

    private IEnumerator TimeWaitThePLayerToBeSeen(float time)
    {
        _playerSeeBehind = true;
        yield return new WaitForSeconds(time);
        _playerSeeBehind = false;
    }
}

