using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPlayer : MonoBehaviour
{
    public bool Ihit = false;

    private void OnTriggerEnter(Collider other)
    {
        Ihit = true;
    }

    private void OnTriggerExit(Collider other)
    {
        Ihit = false;
    }
}
