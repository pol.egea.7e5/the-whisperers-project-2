using System.Collections;
using System.Collections.Generic;
using TMPro.Examples;
using UnityEngine;

public class NonIteractableScript : MonoBehaviour
{
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _waitTillDesapear;
    public AudioClip ratasSound;
 
    private void OnTriggerEnter(Collider other)
    {
        Vector3 direction = (other.transform.position - transform.position).normalized * Random.Range(_speed,_speed*2);
        direction.y = 0;
        for (int i = 0; i < transform.childCount; i++)
        {
            
            transform.GetChild(i).GetComponent<Rigidbody>().velocity = direction;
        }
        StartCoroutine(WaitTillDestroy(_waitTillDesapear));
    }

    public void Death()
    {
       GameManager.Instance.DestroyObj(gameObject);
    }

    private IEnumerator WaitTillDestroy(float time)
    {
        yield return new WaitForSeconds(time);
        Death();
    }

    /// Al tocar un trigger del nivel se har� play en el estado action
    /// Se podran meter transiciones para que haga otras animaciones detras (murcielagos yendose o etc)
    /// En la ultima animaci�n se deber� meter la funcion del EnemyController que hereda de GameManager para destruir el objeto.

}
