using System.Collections.Generic;
using UnityEngine;
using EnumLibrary;
public abstract class EnemyController : MonoBehaviour
{
    //This have to be in a ScriptableObject 
    public float firstDistance;
    public float secondDistance;
    public float firstStressDamage;
    public float SecondStressDamage;
    public  float ThirthStressDamage;
    private float _Proximitydamage;
    public float movementSpeed;//Velocidad personage
    public float attackWaitTime;//Al atacar, tiempo de pausa entre ataques, se pondra un timer que al llegar a ese tiempo vuelva a atacar
    public List<Vector3> patrol;
    private int currentPatrolIndex = 0;
    protected Rigidbody rb;
    public GameObject Target;
    [SerializeField]
    protected LayerMask toIgnore;
    protected EnemyLifeController _eLC;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        rb.isKinematic = false;
        _eLC = GetComponent<EnemyLifeController>();
    }

    protected void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("StressArea"))
        {
            var distance = (other.gameObject.transform.parent.position - gameObject.transform.position).magnitude;
            if (distance <= firstDistance&&distance>secondDistance)
            {
                _Proximitydamage = SecondStressDamage;
            }
            else if (distance <= secondDistance)
            {
                _Proximitydamage = ThirthStressDamage;
            }
            else
            {
                _Proximitydamage = firstStressDamage;
            }
            if (Physics.Raycast(new Ray(transform.position, other.gameObject.transform.parent.position - gameObject.transform.position), out RaycastHit raycastHit, distance, ~toIgnore))
            {
                if (Vector3.Dot(raycastHit.transform.position - transform.position, transform.forward) < 0 && raycastHit.collider.transform.parent.GetComponent<StarterAssets.FirstPersonController>().CrouchAction)
                {
                    _Proximitydamage *= 0.5f;
                }
            }
            other.gameObject.transform.parent.GetComponent<StarterAssets.FirstPersonController>().TakeDamage(_Proximitydamage*Time.deltaTime);
        }
    }
    public void PatrolMovement()
    {
        Vector3 targetPosition = patrol[currentPatrolIndex];
        targetPosition.y = transform.position.y;
       transform.LookAt(targetPosition);
        Vector3 direction = (targetPosition - transform.position).normalized;
        rb.velocity = direction * movementSpeed * Time.deltaTime;

        if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
        {
            currentPatrolIndex = (currentPatrolIndex + 1) % patrol.Count;
        }

        if (currentPatrolIndex == 0 && Vector3.Distance(transform.position, patrol[0]) < 0.1f)
        {
            // si hemos vuelto al punto inicial, volvemos al primer �ndice
            currentPatrolIndex = 1;
        }
    }
    public void PatrolMovement(float inclination)
    {
        Vector3 targetPosition = patrol[currentPatrolIndex];
        targetPosition.y = transform.position.y;
        transform.LookAt(targetPosition);
        transform.Rotate(0,inclination,0);
        Vector3 direction = (targetPosition - transform.position).normalized;
        rb.velocity = direction * movementSpeed * Time.deltaTime;

        if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
        {
            currentPatrolIndex = (currentPatrolIndex + 1) % patrol.Count;
        }

        if (currentPatrolIndex == 0 && Vector3.Distance(transform.position, patrol[0]) < 0.1f)
        {
            // si hemos vuelto al punto inicial, volvemos al primer �ndice
            currentPatrolIndex = 1;
        }
    }
    public void RunMovement(GameObject target)
    {
        Vector3 targetPosition = target.transform.position;
        transform.LookAt(targetPosition);
        Vector3 direction = (targetPosition - transform.position).normalized;
        //rb.MovePosition(transform.position + direction * movementSpeed * Time.fixedDeltaTime);
        rb.velocity = direction * (movementSpeed)*1.5f * Time.deltaTime;
    }
    public void RunMovement(GameObject target,float inclination)
    {
        Vector3 targetPosition = target.transform.position;
        transform.LookAt(targetPosition);
        transform.Rotate(0, inclination, 0);
        Vector3 direction = (targetPosition - transform.position).normalized;
        //rb.MovePosition(transform.position + direction * movementSpeed * Time.fixedDeltaTime);
        rb.velocity = direction * (movementSpeed) * 1.5f * Time.deltaTime;
    }
    public abstract void GetHit();

    public abstract void Death();

}
