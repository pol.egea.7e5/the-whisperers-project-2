using System.Collections;
using System.Collections.Generic;
using EnumLibrary;
using UnityEngine;

public class EnemyQuadrupedPlayerDetector : MonoBehaviour
{
    //If a Target was detected but enemy can't folow him, he can stay alert.
    public bool PlayerDetected=false;
    private bool _playerSeeBehind = false;
    [SerializeField]
    private LayerMask toIgnore;
    [SerializeField]
    private float timeWaitingPlayer;
    private QuadrupedEnemyController _qEC;

    private void Start()
    {
        _qEC = transform.parent.GetComponent<QuadrupedEnemyController>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player")&&!PlayerDetected)
        {
            if (Physics.Raycast(new Ray(transform.parent.position, other.transform.position-transform.parent.position), out RaycastHit hit, 15, ~toIgnore))
            {
                _qEC.StateTransitor(EnemyAnimationActions.Alert);
                transform.parent.gameObject.GetComponent<Animator>().SetBool("Alert", true);
                Debug.Log(hit.collider.gameObject.name);
                if (hit.collider.transform.parent.gameObject==other.gameObject)
                {
                    if (Vector3.Dot(other.transform.position - transform.parent.position, transform.forward) < 0)
                    {
                        Debug.Log("I have to see behind me");
                       StartCoroutine(TimeWaitThePLayerToBeSeen(timeWaitingPlayer));
                    }
                    else
                    {
                        _qEC.Target = other.gameObject;
                        _qEC.StateTransitor(EnemyAnimationActions.Scream);
                        PlayerDetected = true;
                        transform.parent.gameObject.GetComponent<Animator>().SetBool("Detected", true);
                        Debug.Log("Player detected in front of me");
                    }
                }
            }

        }
    }


    private void OnTriggerExit(Collider other)
    {
       if(!PlayerDetected)
        {
            LostPlayer();
        }
    }

    public void LostPlayer()
    {
        transform.parent.gameObject.GetComponent<Animator>().SetBool("Alert", false);
        transform.parent.gameObject.GetComponent<Animator>().SetBool("Detected", false);
        transform.parent.gameObject.GetComponent<Animator>().SetBool("Attack", false);
        _qEC.StateTransitor(EnemyAnimationActions.Walk);
        Debug.Log("PlayerLost");
        _playerSeeBehind = false;
    }

    private IEnumerator TimeWaitThePLayerToBeSeen(float time)
    {
        _qEC.StateTransitor(EnemyAnimationActions.TurnBehind);
        yield return new WaitForSeconds(time);
        if (!PlayerDetected)
        {
            _qEC.StateTransitor(EnemyAnimationActions.Walk);
        }
    }
}

