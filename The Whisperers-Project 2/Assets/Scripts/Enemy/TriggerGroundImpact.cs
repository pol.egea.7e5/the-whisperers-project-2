using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerGroundImpact : MonoBehaviour
{
    private bool _alredyLanded = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ground")&&!_alredyLanded) 
        {
            gameObject.GetComponent<Rigidbody>().useGravity = false;
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            _alredyLanded = true;
        }
    }
}
