using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnumLibrary;

public class ControlIntensity : MonoBehaviour
{
    [SerializeField]
    private LayerMask _wall;
    private Light _light;
    [SerializeField]
    private float _distanceFromWall;
    [SerializeField]
    private float _maxIntensity;
    [SerializeField]
    private float _minIntensity;
    [SerializeField]
    private float _intensityMult;
    public Coroutine audioSound = null;
    public AudioClip EncenderLinterna;
    public AudioClip ApagarLinterna;
    AudioSource AudioSourceLinterna;
    public bool Encendida
    {
        set {_encendida = value;
            audioSound = null;
        }
        get { return _encendida;}
    }
    [SerializeField]
    private bool _encendida;
    private void Start()
    {
        _light = GetComponent<Light>();
        _encendida = true;
        AudioSourceLinterna = gameObject.GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        if(audioSound == null)
        {
           
            if (_encendida)
            {
                audioSound = StartCoroutine(audioWait(EncenderLinterna));

                OpenLight();
            }
            else
            {
                audioSound = StartCoroutine(audioWait(ApagarLinterna));

                CloseLight();
            }
        }

        if (_encendida)
        {
            OpenLight();
        }
        else
        {
            CloseLight();
        }

    }
    private IEnumerator audioWait(AudioClip clip)
    {
        Debug.Log(clip.length);
        AudioSourceLinterna.clip = clip;
        AudioSourceLinterna.Play();
        yield return new WaitForSeconds(clip.length);
    }

    void CloseLight()
    {
        if(_light.intensity>0)
            _light.intensity -= _intensityMult * Time.deltaTime;
    }

    void OpenLight()
    {
        if (_light.intensity < _maxIntensity)
            _light.intensity += _intensityMult * Time.deltaTime;
        else
            _light.intensity = _maxIntensity;
    }
}
