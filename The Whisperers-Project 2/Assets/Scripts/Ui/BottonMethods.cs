using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class BottonMethods : MonoBehaviour
{
    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
    }

    //Carga la escena principal del juego.
    public void LoadScenceGame()
  {
        SceneManager.LoadScene("GameScene");
  }

    // Con este m�todo sales del juego , implementado en el boton Exit.
  public void ExitGame()
  {
        Application.Quit();
  }

   // Carga la escena con la cual , se configura el sonido .
  public void SettingsScence()
  {
        SceneManager.LoadScene("");
  }

    //Carga la escena del menu

  public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
