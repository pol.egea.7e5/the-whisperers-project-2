using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameLoaded : MonoBehaviour
{
    public string path;
    // Start is called before the first frame update
    void Start()
    {
        if (File.Exists(path)) gameObject.SetActive(false);
        else gameObject.SetActive(false);
    }
}
