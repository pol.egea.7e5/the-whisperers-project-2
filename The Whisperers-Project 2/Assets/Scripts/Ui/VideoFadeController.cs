using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoFadeController : MonoBehaviour
{
    public RawImage fadeImage;
    public VideoPlayer videoPlayer;
    public float fadeDuration = 1f;

    private float fadeTimer;
    private bool fadingIn;
    private bool fadingOut;

    void Start()
    {
        fadeImage.color = Color.black;
        fadeTimer = 0f;
        fadingIn = true;
        fadingOut = false;
    }

    void Update()
    {
        if (fadingIn)
        {
            fadeTimer += Time.deltaTime;
            float alpha = 1f - Mathf.Clamp01(fadeTimer / fadeDuration);
            fadeImage.color = new Color(0f, 0f, 0f, alpha);

            if (fadeTimer >= fadeDuration)
            {
                fadingIn = false;
                videoPlayer.Play();
            }
        }

        if (fadingOut)
        {
            fadeTimer += Time.deltaTime;
            float alpha = Mathf.Clamp01(fadeTimer / fadeDuration);
            fadeImage.color = new Color(0f, 0f, 0f, alpha);

            if (fadeTimer >= fadeDuration)
            {
                fadingOut = false;
                videoPlayer.Stop();
                UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
            }
        }
    }

    public void StartFadeOut()
    {
        fadeTimer = 0f;
        fadingOut = true;
    }
}