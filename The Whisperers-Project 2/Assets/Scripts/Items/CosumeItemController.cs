using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CosumeItemController : ItemController
{
    //Idea is acces to GameObject Player send by the inventory to acces at the script parameter you want to modify.
    public abstract void Effect(GameObject Player);
}
