using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLifeController : CharacterLifeController
{
    [SerializeField]
    private float _estresLimit;
    public Image IncrementoDeEstres;
    void Start()
    {

    }

    void Update()
    {
    }

    public override void ModifyLife(float modifier)
    {
        base.ModifyLife(modifier);
        life = Mathf.Clamp(life, 0, _estresLimit);
        IncrementoDeEstres.fillAmount = life / _estresLimit;
        StressManager.Instance.ComproveIfStressInTheLimit(life, _estresLimit);
        if(life>=_estresLimit)
        {
            Death();
        }
    }

    public override void Death()
    {
        GameManager.Instance.EndGame();
    }
    //Delay before death.
}