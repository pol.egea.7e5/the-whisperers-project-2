using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ControlInventory : MonoBehaviour
{
    [SerializeField]
    private Inventory inventory;
    private int _weaponCounter;
    private int _itemCounter;
    private ChangeWeaponController _changeWeapon;
    private WeaponSpawnerController _weaponSpawner;
    private PlayerInput _playerInput;
    private InputAction _openMenu;
    private InputAction _grabWeapon;
    private InputAction _closeMenu;
    private IEnumerator _weaponCurrentTimeDeseaper;
    private bool _weaponDispatch;
    // Start is called before the first frame update
    void Start()
    {
        _playerInput = transform.parent.GetComponent<PlayerInput>();
        _openMenu = _playerInput.actions["OpenWeaponMenu"];
        _closeMenu = _playerInput.actions["ExitWeaponMenu"];
        _grabWeapon = _playerInput.actions["GrabWeapon"];
        inventory.Weapons = new List<WeaponData>();
        inventory.Items = new List<ItemData>();
        _weaponCounter = 0;
        _itemCounter = 0;
        TryGetComponent(out _changeWeapon);
         try
         {
             GameObject.Find("WeaponSpawner").TryGetComponent(out _weaponSpawner);
         }
         catch 
         {
             Debug.LogError("Error: There is no WeaponSpawner in the scene");
         }
         if(inventory.Weapons.Count==0)
         {
             _openMenu.started += SearchWeapon;
         }
         _weaponDispatch = false;
    }

    private void Update()
    {
        if (_weaponDispatch && _weaponCurrentTimeDeseaper != null)
        {
            StopCoroutine(_weaponCurrentTimeDeseaper);
        }
    }

    private bool AddWeapon(WeaponData weaponData)
    {
        var cloneWeapon = Instantiate(weaponData);
        if (inventory.LimitWeapons > inventory.Weapons.Count)
        {
            if (!IsInTheList(weaponData))
            {
                inventory.Weapons.Add(cloneWeapon);
                if (inventory.Weapons.Count == 1)
                {
                    SetWeaponValues(inventory.Weapons[_weaponCounter]);
                }
                return true;
            }
        }
        else
        {
            Debug.Log("No more space");
            return true;
        }
        return false;
    }

    private void AddEquipableItem()
    {

    }

    public void AddItem(ItemData itemData)
    {
        if (itemData is WeaponData weaponData)
            AddWeapon(weaponData);
        else if (itemData is CosumeItemData cosumeItemData)
            cosumeItemData.Script.Effect(transform.parent.gameObject);
        else
            AddEquipableItem();

    }
    public void ChangeWeapon()
    {
        if (inventory.Weapons.Count > 0)
        {
                _weaponCounter += 1;
                if (inventory.Weapons.Count <= _weaponCounter)
                    _weaponCounter = 0;
                SetWeaponValues(inventory.Weapons[_weaponCounter]);
        }
    }

    public void ObtainWeapon(InputAction.CallbackContext context)
    {
        Debug.Log("WeaponObtained");
        //Only on Search this is active. Desactive SearchWeaponAction when you obtain the weapon.
        AddWeapon(_weaponSpawner.GiveActualWeapon());
        //Desactivate Subcription
        ExitSearchMenu(context);
        _openMenu.started -= SearchWeapon;
        _weaponCurrentTimeDeseaper = WaitTimeTillDisapear(inventory.Weapons[0].UseTime);
     //   StartCoroutine(_weaponCurrentTimeDeseaper);
        _weaponDispatch = false;
    }

    public void SearchWeapon(InputAction.CallbackContext context)
    {
        Debug.Log("Menu");
      //Every Q press is a new current weapon. 
      _weaponSpawner.SearchWeapon();
       _grabWeapon.started += ObtainWeapon;
       _closeMenu.started += ExitSearchMenu;
       //UI menu.
    }

    public void ExitSearchMenu(InputAction.CallbackContext context)
    {
        _weaponSpawner.ExitMenu();
        Debug.Log("CloseMenu");
        _grabWeapon.started -= ObtainWeapon;
        _closeMenu.started -= ExitSearchMenu;
        GameManager.Instance.DestroyActualWeaponMenu();
        //UI menu close.
    }

    public void DetachTheCurrentWeapon()
    {
        _changeWeapon.DetachCurrentWeaponData();
        inventory.Weapons.Clear();
        //Active SearchWeaponAction
        _openMenu.started += SearchWeapon;
        _weaponDispatch = true;
        Debug.Log("Weapon Disapear");
        _changeWeapon.AnimationWeights(EnumLibrary.WeaponAnimType.NoAnmation);
    }
    private bool IsInTheList(WeaponData weaponData)
    {
        foreach (var weapon in inventory.Weapons)
            if (weapon.Name == weaponData.Name)
            {
                /*  weapon.shootData.TotalBullets += weaponData.shootData.TotalBullets;
                  weapon.shootData.currentBullets = weapon.shootData.TotalBullets;*/
                return true;
            }
        return false;
    }
    public void SetWeaponValues(WeaponData newweapon)
    {
        _changeWeapon.SetNewWeaponData(newweapon);
    }

    private IEnumerator WaitTimeTillDisapear(float time)
    {
        yield return new WaitForSeconds(time);
        Debug.Log("Weapon Disapear By Time");
        DetachTheCurrentWeapon();
        _weaponCurrentTimeDeseaper = null;
    }
}
