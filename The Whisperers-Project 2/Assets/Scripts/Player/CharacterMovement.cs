using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CharacterMovement : MonoBehaviour
{
        [SerializeField] private float movementSpeed = 5f;
        [SerializeField] private float lookSensitivity = 3f;
        [SerializeField] private float jumpForce = 5f;

        private Rigidbody rb;
        private Camera cam;
        private float verticalRotation = 0f;
        private Vector3 moveDirection = Vector3.zero;
        private bool isGrounded = false;

        private void Start()
        {
            rb = GetComponent<Rigidbody>();
            cam = GetComponentInChildren<Camera>();
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void Update()
        {
            // Obtener la entrada del jugador
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");
            bool jump = Input.GetButtonDown("Jump");
            gameObject.GetComponent<Animator>().SetFloat("movement",rb.velocity.x);
            // Girar el objeto horizontalmente
            transform.Rotate(Vector3.up * mouseX * lookSensitivity);

            // Girar la c�mara verticalmente
            verticalRotation -= mouseY * lookSensitivity;
            verticalRotation = Mathf.Clamp(verticalRotation, -90f, 90f);
            cam.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);

            // Crear un vector de movimiento en funci�n de la entrada del jugador
            moveDirection = transform.TransformDirection(new Vector3(horizontal, 0, vertical));
            moveDirection *= movementSpeed;

            // Saltar si el jugador est� en el suelo y ha pulsado el bot�n de saltar
            if (isGrounded && jump)
            {
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            }
        }

        private void FixedUpdate()
        {
            // Mover el personaje en la direcci�n del vector de movimiento
            rb.MovePosition(rb.position + moveDirection * Time.fixedDeltaTime);
        }

        private void OnCollisionStay(Collision collision)
        {
            // Comprobar si el personaje est� en el suelo
            isGrounded = true;
        gameObject.GetComponent<Animator>().SetBool("jump", false);
        foreach (ContactPoint contact in collision.contacts)
            {
                if (Vector3.Dot(contact.normal, Vector3.up) < 0.5f)
                {
                    isGrounded = false;
                    break;
                }
            }
        }

        private void OnCollisionExit()
        {
        gameObject.GetComponent<Animator>().SetBool("jump", true);
        // El personaje ya no est� en el suelo
        isGrounded = false;
        }
}
