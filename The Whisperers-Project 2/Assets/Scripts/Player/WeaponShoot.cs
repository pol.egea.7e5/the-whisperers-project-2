using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponShoot : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform SpawnBullet;
    public GameObject Arma;
    public GameObject Torso;

    public float shotForce = 1500;
    public float shotRace = 40f;

    public float shotRateTime = 0;

    void Update()
    {
        Vector3 positionSpawn = Camera.main.transform.position;
        Quaternion rotacionSpawn = Camera.main.transform.rotation;

        SpawnBullet.position = positionSpawn;
        SpawnBullet.rotation = rotacionSpawn;

        if (Input.GetButton("Fire1"))
        {
            if(Time.time > shotRateTime)
            {
                GameObject newbullet;

                newbullet = Instantiate(bulletPrefab, SpawnBullet.position+ SpawnBullet.transform.forward * 0.5f, SpawnBullet.rotation);
                //newbullet.GetComponent<Rigidbody>().AddForce(SpawnBullet.transform.forward * shotRace);
                newbullet.GetComponent<Rigidbody>().velocity = new Vector3( SpawnBullet.transform.forward.x, SpawnBullet.transform.forward.y, SpawnBullet.transform.forward.z ) * shotRace;
                shotRateTime = Time.time + shotRace;

                Destroy(newbullet, 2);
            }
        }
    }
}
