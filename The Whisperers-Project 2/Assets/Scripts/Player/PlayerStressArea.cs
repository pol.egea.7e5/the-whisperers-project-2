using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStressArea : MonoBehaviour
{
    public PlayerLifeController PlayerLifeController;
    private bool haPerdidoVida = false;
    [SerializeField]
    private float _recoverStress;
    void Start()
    {
        
    }

     void Update()
    {
        if (haPerdidoVida&& PlayerLifeController.life > 0)
        {
            
            PlayerLifeController.ModifyLife(_recoverStress* Time.deltaTime*-1);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy")) 
        {
            haPerdidoVida = false;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {

            haPerdidoVida = true;            
            Debug.Log("Estres restante: " + PlayerLifeController.life);
        }
    }

}
