using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueEmpujePlayer : MonoBehaviour
{
    public Animator animation; 
    public float distanciaEmpuje =2f;
    public float fuerzaEmpuje = 2f;
    public CapsuleCollider ColliderForPull;

    void OnTriggerEnter(Collider other)
    {
        
       if (other.gameObject.CompareTag("Enemy"))
       {
         EmpujarEnemigo(other.transform);
       }
    }

    void EmpujarEnemigo(Transform enemigo)
    {
            Vector3 direccionEmpuje = (enemigo.position - transform.position).normalized;
            direccionEmpuje.y = 0;
            enemigo.position += direccionEmpuje * distanciaEmpuje;
            enemigo.GetComponent<Rigidbody>().AddForce(direccionEmpuje * fuerzaEmpuje, ForceMode.Impulse);
        animation.SetTrigger("Punch");
    }

}
