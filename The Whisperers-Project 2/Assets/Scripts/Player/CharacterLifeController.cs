using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterLifeController : MonoBehaviour
{
    public float life = 0;
    void Start()
    {
    }


    void Update()
    {
    }


    public virtual void ModifyLife(float modifier)
    {
        life += modifier;
    }

    public abstract void Death();
}
