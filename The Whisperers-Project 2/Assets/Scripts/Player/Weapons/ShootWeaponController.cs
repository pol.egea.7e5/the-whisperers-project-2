using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootWeaponController : WeaponController
{
    private ShootWeaponData _shootWeaponData;
    private GameObject _spawnBullet;
    AudioSource AudioSourceWeapon;
    public AudioClip SonidoDisparo;
    protected override void Start()
    {
        _spawnBullet = Camera.main.transform.Find("BulletSpawn").gameObject;
        base.Start();
        _shootWeaponData = (ShootWeaponData)WeaponData;

        //StressManager.Instance.OnStressLevelChange += AttackOnMediumStressLevel;
        //StressManager.Instance.OnStressLevelChange += AttackOnHighStressLevel;
        AudioSourceWeapon = gameObject.GetComponent<AudioSource>();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void Attack()
    {
        _shootWeaponData.Anim.AnimationAction(EnumLibrary.AnimationActions.Attack);
        AudioSourceWeapon.clip = SonidoDisparo;
        AudioSourceWeapon.Play();
        GameObject newbullet;
        newbullet = Instantiate(_shootWeaponData.ProyectilePrefab, _spawnBullet.transform.position + _spawnBullet.transform.forward * 0.5f, _spawnBullet.transform.rotation);
        newbullet.GetComponent<Rigidbody>().velocity = new Vector3(_spawnBullet.transform.forward.x, _spawnBullet.transform.forward.y, _spawnBullet.transform.forward.z) * _shootWeaponData.ProyectileSpeed;
        newbullet.GetComponent<BulletCrash>().Damage = _shootWeaponData.ProyectileDamage;
    }

    /// <summary>
    /// Cuando el estres esta en un nivel medio falla el 50% de las balas
    /// </summary>
    /// <param name="levelOfStrees"></param>
    public void AttackOnMediumStressLevel(EnumLibrary.LevelOfStrees levelOfStrees)
    {

        _shootWeaponData.Anim.AnimationAction(EnumLibrary.AnimationActions.Attack);
        GameObject newbullet;
        newbullet = Instantiate(_shootWeaponData.ProyectilePrefab, _spawnBullet.transform.position + _spawnBullet.transform.forward * 0.5f, _spawnBullet.transform.rotation);

        // add randomness to bullet trajectory
        bool percentFallo = Random.value < 0.5f;
        if (percentFallo)
        {
            // miss the Target
            newbullet.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        else
        {
            // hit the Target
            newbullet.GetComponent<Rigidbody>().velocity = new Vector3(_spawnBullet.transform.forward.x, _spawnBullet.transform.forward.y, _spawnBullet.transform.forward.z) * _shootWeaponData.ProyectileSpeed;
        }
    }

    /// <summary>
    /// Cuando el estres esta en un nivel medio falla el 80% de las balas
    /// </summary>
    /// <param name="levelOfStrees"></param>
    public void AttackOnHighStressLevel(EnumLibrary.LevelOfStrees levelOfStrees)
    {
        _shootWeaponData.Anim.AnimationAction(EnumLibrary.AnimationActions.Attack);
        GameObject newbullet;
        newbullet = Instantiate(_shootWeaponData.ProyectilePrefab, _spawnBullet.transform.position + _spawnBullet.transform.forward * 0.5f, _spawnBullet.transform.rotation);

        bool percentFallo = Random.value <= 0.8f;
        if (percentFallo)
        {
            
            newbullet.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        else
        {
            newbullet.GetComponent<Rigidbody>().velocity = new Vector3(_spawnBullet.transform.forward.x, _spawnBullet.transform.forward.y, _spawnBullet.transform.forward.z) * _shootWeaponData.ProyectileSpeed;
        }
    }

}

