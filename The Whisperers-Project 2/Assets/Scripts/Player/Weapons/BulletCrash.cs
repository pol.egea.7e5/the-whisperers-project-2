using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCrash: MonoBehaviour
{
    public GameObject ParticulasColisionPared;
    public float Damage;
    public AudioClip crashWall;
    public AudioClip crashEnemy;
    public AudioSource AudioBullet;
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<EnemyLifeController>().ModifyLife(Damage*-1);
            AudioBullet.clip = crashWall;
            AudioBullet.Play();
            Instantiate(ParticulasColisionPared, transform.position, Quaternion.Inverse(transform.rotation));
            //Destroy(gameObject);
        }

        if (collision.collider.gameObject.layer == 9)
        {

            Debug.Log("Enemigo tocado");
            AudioBullet.clip = crashEnemy;
            AudioBullet.Play();
            //collision.transform.GetComponent<EnemyLifeController>().ModifyLife(20);
            Destroy(gameObject);
        }
        Destroy(gameObject);
    }

}
