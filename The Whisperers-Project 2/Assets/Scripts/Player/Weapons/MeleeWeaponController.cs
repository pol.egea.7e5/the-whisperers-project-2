using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnumLibrary;

public class MeleeWeaponController : WeaponController
{
    private MeleeWeaponData _meleeWeapon;
    private Collider _collider;
    protected override void Start()
    {
        base.Start();
        _meleeWeapon = (MeleeWeaponData)WeaponData;
        _collider = GetComponent<Collider>();
        DesactiveCollider();
    }

    public override void Attack()
    {
        WeaponData.Anim.AnimationAction(AnimationActions.Attack);
    }

    void EmpujarEnemigo(Transform enemigo)
    {
        Vector3 direccionEmpuje = (enemigo.position - transform.position).normalized;
        direccionEmpuje.y = 0;
        direccionEmpuje.x = 0;
        enemigo.GetComponent<Rigidbody>().AddForce(direccionEmpuje * _meleeWeapon.HitImpulse, ForceMode.Impulse);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            EmpujarEnemigo(other.transform);
            other.gameObject.GetComponent<EnemyLifeController>().ModifyLife(_meleeWeapon.Damage * -1);
        }
    }

    public void ActiveCollider()
    {
        _collider.enabled= true;
    }

    public void DesactiveCollider()
    {
        _collider.enabled= false;
    }
}
