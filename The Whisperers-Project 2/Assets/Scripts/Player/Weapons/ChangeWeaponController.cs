using UnityEngine;
using EnumLibrary;
using UnityEngine.InputSystem;
using System.Collections;

public class ChangeWeaponController : MonoBehaviour
{
    private GameObject Player;
    private GameObject Weapon;
    private AnimationController _playerAnimController;
    private void Start()
    {
        Player = GameObject.Find("Model");
        Player.TryGetComponent(out _playerAnimController);
        Weapon = GameObject.Find("WeaponHolder");
    }
    public void SetNewWeaponData(WeaponData weaponData)
    {
        GameObject newWeapon = Instantiate(weaponData.prefab);
        if (Weapon.transform.childCount > 0)
            DetachCurrentWeaponData();
        AnimationWeights(weaponData.AnimType);
        newWeapon.TryGetComponent<WeaponController>(out var weaponController);
        weaponController.WeaponData = weaponData;
        transform.parent.TryGetComponent<PlayerInput>(out var playerInput);
        weaponController._weaponAttack = playerInput.actions["WeaponAttack"];
        weaponController._DetachWeapon = playerInput.actions["DetachWeapon"];
        weaponController._controlInventory = GetComponent<ControlInventory>();
        ParentAndChildrenMethods.ParentAChildren(Weapon, newWeapon);
        newWeapon.transform.SetLocalPositionAndRotation(weaponData.Position, weaponData.Rotation);
        Debug.Log(newWeapon.transform.localRotation);
        Debug.Log(newWeapon.transform.localPosition);
        ////This next code lines is because this positioning don't actual work in the build for some reason.
        switch (weaponData.AnimType)
        {
            case WeaponAnimType.PunchWeapon:
                newWeapon.transform.SetLocalPositionAndRotation(new Vector3(0.02f, -0.05f, -0.01f), new Quaternion(0.45156f, 0.02764f, 0.80438f, -0.38510f));
                break;
            case WeaponAnimType.OneHandedFireWeapon:
                newWeapon.transform.SetLocalPositionAndRotation(new Vector3(0.09f, -0.03f, -0.07f), new Quaternion(-0.20146f, -0.07670f, -0.97396f, -0.07029f));
                break;
            case WeaponAnimType.SwordMeleeWeapon:
                newWeapon.transform.SetLocalPositionAndRotation(new Vector3(-0.04f, -0.04f, 0.09f), new Quaternion(-0.68408f, 0.05052f, -0.36836f, 0.62753f));
                break;
        }
        weaponData.Anim = _playerAnimController;
        weaponData.EventSubscriber = true;
    }

    public void DetachCurrentWeaponData()
    {
        GameObject actualWeapon = Weapon.transform.GetChild(0).gameObject;
        Destroy(actualWeapon.GetComponent<WeaponController>());
        Destroy(actualWeapon);
    }

    public void AnimationWeights(WeaponAnimType AnimType)
    {
        var animLayerWeapon = AnimType switch
        {
            WeaponAnimType.PunchWeapon => 1,
            WeaponAnimType.SwordMeleeWeapon => 2,
            WeaponAnimType.OneHandedFireWeapon => 3,
            WeaponAnimType.TwoHandedFireWeapon => 4,
            _ => 0,
        };
        for (int i = 1; i < 5; i++)
        {
            if (i == animLayerWeapon)
                StartCoroutine(WaitTillAnimationEnd(i,1,1));
            else
                StartCoroutine(WaitTillAnimationEnd(i, 0, 1));
        }
    }

    private IEnumerator WaitTillAnimationEnd(int layer, float weight,float speed)
    {
        while(_playerAnimController.AnimTransition)
        yield return null;
        _playerAnimController.SetAnimationLayerWeight(layer, weight, speed);
    }
}
