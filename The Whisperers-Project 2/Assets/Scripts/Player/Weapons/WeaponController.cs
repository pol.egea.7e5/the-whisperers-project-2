using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class WeaponController : StateController
{
    public WeaponData WeaponData;
    private ScriptableState _attack;
    private ScriptableState _countDown;
    private ScriptableState _idle;
    public InputAction _weaponAttack;
    public InputAction _DetachWeapon;
    public ControlInventory _controlInventory;
    protected virtual void Start()
    {
        WeaponData.Script = this;
        _idle = WeaponData.States[0];
        _countDown = WeaponData.States[1];
        _attack = WeaponData.States[2];
        currentState.Action.weaponData = WeaponData;
    }
    public void Idle()
    {
        ChangeStateWeapon(_idle);
    }
    public void WeaponAttack(InputAction.CallbackContext context)
    {
        ChangeStateWeapon(_attack);
    }

    public void CountDown()
    {
        ChangeStateWeapon(_countDown);
    }

    public abstract void Attack();

    //public abstract void AttackAttackOnMediumStressLevel();


    private void ChangeStateWeapon(ScriptableState state)
    {
        state.Action.weaponData = WeaponData;
        StateTransitor(state);
    }

    protected void Recoil()
    {
        //Recoil of the weapon
    }

    public void WeaponWeight()
    {
        //WeightOfTheWeaponThatAffectsPlayerSpeed
    }

    public void WaitTime(float time)
    {
        StartCoroutine(Countdown(time));
    }

    private IEnumerator Countdown(float time)
    {
        yield return new WaitForSeconds(time);
        WeaponData.StateIsActive = false;
    }

    public void DetachWeapon(InputAction.CallbackContext context)
    {
        _controlInventory.DetachTheCurrentWeapon();
    }

    private void OnDestroy()
    {
      _weaponAttack.started -= WeaponAttack;
      _DetachWeapon.started -=DetachWeapon;
    }
}

