using UnityEngine;
using EnumLibrary;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.IO;
using System.Collections;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Game Manager is NULL");
            }
            return _instance;
        }
    }

    [SerializeField]
    private Transform _player;
    [SerializeField]
    private EnemyBossController _eBC;
    [SerializeField]
    private float _distanceBossPlayer;
    //For Saving
    public string pathSave;
    public List<GameObject> Checkpointorder;
    public List<bool> checkpointspassed;
    public int kills;

    public GameObject playerInGame;
    public GameObject remember; 
    AudioSource AudioSourcePlayer;
    public AudioClip ScreamDeath;
    [SerializeField]
    private GameObject WeaponHolderInCanvas;

    private void Awake()
    {
        if (_instance != null)
            Destroy(gameObject);
        else
        {
            //DontDestroyOnLoad(gameObject);
            _instance = this;
        }
       Cursor.lockState = CursorLockMode.Locked;
    }
    public void Start()
    {
        if(PlayerPrefs.GetInt("New")==1&& File.Exists(pathSave)) LoadSave();

    }

    public void RoomSenseChanged(CorridorDirection direction)
    {
        Vector3 enemyBossPosition;
        Quaternion bossRotation = new(0, 0, 0, 0);
        switch (direction)
        {
            case CorridorDirection.BackToFront:
                enemyBossPosition = new Vector3(_player.position.x,_player.position.y,_player.position.z-_distanceBossPlayer);
                break;
            case CorridorDirection.FrontToBack:
                enemyBossPosition = new Vector3(_player.position.x, _player.position.y, _player.position.z + _distanceBossPlayer);
                break;
            case CorridorDirection.LeftToRight:
                enemyBossPosition = new Vector3(_player.position.x -_distanceBossPlayer, _player.position.y, _player.position.z);
                bossRotation = new Quaternion(0,90,0,-90);
                break;
            case CorridorDirection.RightToLeft:
                enemyBossPosition = new Vector3(_player.position.x +_distanceBossPlayer, _player.position.y, _player.position.z);
                bossRotation = new Quaternion(0,-90, 0, 90);
                break;
            default:
                enemyBossPosition = new Vector3(_player.position.x, _player.position.y, _player.position.z - _distanceBossPlayer);
                break;
        }
        _eBC.ChangePosition(enemyBossPosition,bossRotation);
    }

    public void EndGame()
    {
        AudioSourcePlayer = playerInGame.GetComponent<AudioSource>();

        playerInGame.GetComponentInChildren<Animator>().SetBool("Death", true);


        AudioSourcePlayer.clip = ScreamDeath;
        AudioSourcePlayer.Play();

        remember.SetActive(true);
        StartCoroutine(LoadingGameOver());
    }

    public void WinGame()
    {
        SceneManager.LoadScene("WinScene");
    }

    public IEnumerator LoadingGameOver()
    {
        yield return new WaitForSeconds(6);
        SceneManager.LoadScene("GameOver");
    }

    public void SpawnPlayer()
    {

        // Encuentra el ultimo checkpoint activo
        CheckpointController checkpointController = FindObjectOfType<CheckpointController>();
        Vector3 respawnPosition = checkpointController.checkpointPosition;
        Quaternion respawnRotation = checkpointController.checkpointRotation;

        GameObject checkSpawn = Checkpointorder[checkpointspassed.LastIndexOf(true)];

        // Hace spawn del jugador en el ultimo checkpoint activo
        GameObject newPlayer = Instantiate(/*playerPrefab*/ this.gameObject, checkSpawn.transform.position+Vector3.forward+Vector3.up, checkSpawn.transform.rotation);     //S'ha de pasar la INFO del PLAYER
                                                                                                                    //Incloent el prefab per SO( scriptable Object)
        newPlayer.SetActive(true);
    }

    public void LoadSave()
    {
        if (File.Exists(pathSave))
        {
            string[] file = File.ReadAllText(pathSave).Split("/");
            Debug.Log(file[0]);
            checkpointspassed = JsonUtility.FromJson<List<bool>>(file[1]);
            kills = System.Convert.ToInt32(file[2]);
            for (int i=checkpointspassed.Count-1;i<=0;i--)
            {
                Checkpointorder[i].SetActive(checkpointspassed[i]);
            }
            SpawnPlayer();
        }
    }

    public void DestroyObj(GameObject obj)
    {
        Destroy(obj);
    }

    public void SetWeaponInMenu(WeaponData weaponData)
    {
        DestroyActualWeaponMenu();
        GameObject weaponUI = Instantiate(weaponData.UIPrefab, WeaponHolderInCanvas.transform);
        ParentAndChildrenMethods.ParentAChildren(WeaponHolderInCanvas, weaponUI);
        weaponUI.transform.SetLocalPositionAndRotation(weaponData.UIPosition, weaponData.UIRotation);
    }

    public void DestroyActualWeaponMenu()
    {
        Debug.Log(WeaponHolderInCanvas.transform.childCount);
        if (WeaponHolderInCanvas.transform.childCount > 0)
        {
            for (int i = 0; i < WeaponHolderInCanvas.transform.childCount; i++)
                Destroy(WeaponHolderInCanvas.transform.GetChild(i).gameObject);
        }
    }
}

