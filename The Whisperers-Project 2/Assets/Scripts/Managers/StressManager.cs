using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StarterAssets;
using EnumLibrary;
using System.IO;

public class StressManager : MonoBehaviour
{
    private bool _sendStress;
    private FirstPersonController _fPC;
    private PlayerLifeController _playerLifeController;
    private GameObject _player;
    [SerializeField]
    private Light _playerLight;
    [SerializeField]
    private float _temperatureMult;
    [SerializeField]
    private float _temperatureMin;
    [SerializeField]
    private float _highStress;
    [SerializeField]
    private float _mediumStress;
    public float CurrentStrees;
    //This event is active when a Stress level is trasspaset. From low to medium to high. This is going to be useful for the emergency state. 
    public delegate void StreesLevelChange(LevelOfStrees levelOfStrees);
    public event StreesLevelChange OnStressLevelChange;
    private static StressManager _instance;
    public static StressManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Stress Manager is NULL");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        Debug.Log("Awake");
        if (_instance != null)
            Destroy(gameObject);
        else
        {
           // DontDestroyOnLoad(gameObject);
            _instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _sendStress = true;
        _player = GameObject.Find("Player");
        //_fPC = _player.GetComponent<FirstPersonController>();
        _playerLifeController = _player.GetComponent<PlayerLifeController>();
        CurrentStrees = _playerLifeController.life;
    }

    // Update is called once per frame
    void Update()
    {
        if (_sendStress)
        {
            SendStressLevel(CurrentStrees);
            _sendStress = false;
        }
    }

    private void SendStressLevel(float stress)
    {
        if (stress <= _mediumStress)
            OnStressLevelChange(LevelOfStrees.Low);
        else if (stress > _mediumStress && stress <= _highStress)
            OnStressLevelChange(LevelOfStrees.Medium);
        else if (stress > _highStress)
            OnStressLevelChange(LevelOfStrees.High);
    }

    public void ComproveIfStressInTheLimit(float stress, float limitStress)
    {
        CurrentStrees = stress;
        SendStressLevel(stress);
    }
}
