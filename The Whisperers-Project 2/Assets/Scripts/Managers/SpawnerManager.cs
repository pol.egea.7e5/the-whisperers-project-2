using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    public GameObject enemy;
    public float timer = 300;

    [SerializeField]
    private float counter;
    [SerializeField]
    private List<Vector3> _patrolForEnemies;

    // Start is called before the first frame update
    void Start()
    {
        counter = timer;
       GameObject enemyInstant = Instantiate(enemy,transform.position,transform.rotation,transform);
       SetPatrol(enemyInstant);
    }

    // Update is called once per frame
    void Update()
    {
        if (counter <= 0 && transform.childCount == 0)
        {
            GameObject enemyInstant = Instantiate(enemy, transform.position, transform.rotation, transform);
            SetPatrol(enemyInstant);
            counter = timer;
        }
        else if (transform.childCount == 0) counter -= Time.deltaTime;
    }

    void SetPatrol(GameObject enemy)
    {
        enemy.GetComponent<EnemyController>().patrol = _patrolForEnemies;
    }
}
