using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoPlayerManager : MonoBehaviour
{
    bool makeit = false;
    public VideoPlayer videoPlayer;
    double duration;
    public VideoFadeController fade;

    void Start()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        videoPlayer.Play();
        videoPlayer.renderMode = VideoRenderMode.CameraFarPlane;
        videoPlayer.targetCameraAlpha = 1f;
        videoPlayer.aspectRatio = VideoAspectRatio.FitOutside;
        duration = videoPlayer.clip.length;
    }

    // Update is called once per frame
    void Update()
    {
        duration -= System.Convert.ToDouble(Time.deltaTime);
       if(duration- System.Convert.ToDouble(fade.fadeDuration) <= 0&&!makeit)
        {
            makeit = true;
            fade.StartFadeOut();
        }
    }
}
