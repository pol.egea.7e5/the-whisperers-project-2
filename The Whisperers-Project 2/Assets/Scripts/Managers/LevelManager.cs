using System.Collections.Generic;
using UnityEngine;
using EnumLibrary;

public class LevelManager : MonoBehaviour
{
    private int _currenRoomId;
    //La idea, es que cada cambio de nivel esta Lista cambie a unas nuevas rooms. A aquellas rooms dentro de ese nuevo nivel.
    [SerializeField]
    private List<GameObject> Rooms;
    private CorridorDirection _currentCD;
    // Start is called before the first frame update
    void Start()
    {
        VinculateAllCurrentRooms();
        _currenRoomId = Rooms[0].GetComponent<RoomController>().RoomId;
        ChangeBossPosition(Rooms[_currenRoomId].GetComponent<RoomController>().Direction);
    }
    void RoomChanged(RoomController RC)
    {
        Debug.Log("He entrado");
        if (_currenRoomId != RC.RoomId)
        {
            _currenRoomId = RC.RoomId;
            if (RC.Direction != _currentCD)
            {
                ChangeBossPosition(RC.Direction);
                _currentCD = RC.Direction;
            }
        }
        else
        {
            _currenRoomId -= 1;
            if (_currenRoomId < 0)
                _currenRoomId = 0;
            ChangeBossPosition(Rooms[_currenRoomId].GetComponent<RoomController>().Direction);
            _currentCD = Rooms[_currenRoomId].GetComponent<RoomController>().Direction;
        }
    }

   void EliminateAllCurrentEvents()
    {
        foreach(var room in Rooms)
        {
            DesvinculateEvent(room.GetComponent<RoomController>());
        }
    }

    void VinculateAllCurrentRooms()
    {
        int i = 0;
        foreach (var room in Rooms)
        {
            room.GetComponent<RoomController>().RoomId = i;
            VinculateEvent(room.GetComponent<RoomController>());
            i += 1;
        }
    }

    void ChangeBossPosition(CorridorDirection CD)
    {
        GameManager.Instance.RoomSenseChanged(CD);
    }

    void VinculateEvent(RoomController RC)
    {
        RC.OnEnterRoom += RoomChanged;
    }

    void DesvinculateEvent(RoomController RC)
    {
        RC.OnEnterRoom -= RoomChanged;
    }
}
