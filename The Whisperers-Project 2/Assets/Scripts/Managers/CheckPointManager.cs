using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointManager : MonoBehaviour
{
    public Vector3 checkpointPosition;
    public Quaternion checkpointRotation;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            checkpointPosition = other.transform.position;
            checkpointRotation = other.transform.rotation;
        }
    } 
}

