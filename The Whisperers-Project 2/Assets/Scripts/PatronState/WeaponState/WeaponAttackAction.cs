using UnityEngine;
using EnumLibrary;

[CreateAssetMenu(fileName = "WeaponAttackAction", menuName = "WeaponAttackAction")]
public class WeaponAttackAction : ScriptableAction
{
    public override void OnFinishedState()
    {
    }

    public override void OnSetState()
    {
        base.OnSetState();
        weaponData.Anim.AnimationAction(AnimationActions.Attack);
        weaponData.Script.Attack();
        weaponData.Script.WaitTime(weaponData.AttackDuration);
    }

    public override void OnUpdate()
    {
        if (!weaponData.StateIsActive)
            weaponData.Script.CountDown();
    }
}
