using UnityEngine;

[CreateAssetMenu(fileName = "WeaponCountDownState", menuName = "WeaponCountDownState")]
public class WeaponCountDownState : ScriptableAction
{
    public override void OnFinishedState()
    {
       // weaponData.Anim.SetBool("ComeBack", false);
    }

    public override void OnSetState()
    {
        base.OnSetState();
        weaponData.Anim.AnimationAction(EnumLibrary.AnimationActions.Countdown);
        weaponData.Script.WaitTime(weaponData.Countdown);
    }

    public override void OnUpdate()
    {
        if (!weaponData.StateIsActive)
            weaponData.Script.Idle();
    }
}
