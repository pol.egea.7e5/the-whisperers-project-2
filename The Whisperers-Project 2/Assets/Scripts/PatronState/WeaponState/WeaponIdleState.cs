using UnityEngine;
using UnityEngine.InputSystem;

[CreateAssetMenu(fileName = "WeaponIdleState", menuName = "WeapondIdleState")]
public class WeaponIdleState : ScriptableAction
{
    public override void OnFinishedState()
    {
        weaponData.Script._weaponAttack.started -= weaponData.Script.WeaponAttack;
        weaponData.Script._DetachWeapon.started -= weaponData.Script.DetachWeapon;
       // weaponData.Anim.SetBool("Idle", false);
    }

    public override void OnUpdate()
    {
        if (weaponData.EventSubscriber)
        {
            weaponData.Script._weaponAttack.started += weaponData.Script.WeaponAttack;
            weaponData.Script._DetachWeapon.started += weaponData.Script.DetachWeapon;
            weaponData.EventSubscriber = false;
        }

    }
    public override void OnSetState()
    {
        weaponData.EventSubscriber = true;
        //weaponData.Anim.SetBool("Idle", true);
    }
}
