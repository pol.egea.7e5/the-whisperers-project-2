using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalDoorOpen : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("touched finaldoor");
        //Act Condition:
        if(collision.collider.tag=="Player")
        transform.parent.gameObject.GetComponent<Animator>().SetBool("open", true);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("touched finaldoor");
        //Act Condition:
        if (other.GetComponent<Collider>().tag == "Player")
            transform.parent.gameObject.GetComponent<Animator>().SetBool("open", true);
    }

}
