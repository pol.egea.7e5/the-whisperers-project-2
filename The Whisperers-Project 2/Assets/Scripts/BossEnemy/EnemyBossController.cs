using StarterAssets;
using UnityEngine;

public class EnemyBossController : MonoBehaviour
{
    private FirstPersonController _fPC;
    private Rigidbody _rb;
    [SerializeField]
    private float _speedMultiplicator;
    [SerializeField]
    private GameObject _player;
    private AudioSource SonidoEnemyBoss;
    bool markpassed;
    private void Start()
    {
        _rb = gameObject.GetComponent<Rigidbody>();
        _fPC = _player.GetComponent<FirstPersonController>();
    }

    void FixedUpdate()
    {
      Move(_fPC.Speed.velocity.magnitude > 0 ? _fPC.Speed.velocity * _speedMultiplicator : Vector3.zero, _fPC.Speed.velocity.magnitude > 0 ? new Vector3(_fPC.InputDirection.x, 0, _fPC.InputDirection.z).normalized : Vector3.zero);

        if(markpassed == true)
        {
            SonidoEnemyBoss.Play();
        }
    }

    void Move(Vector3 speed, Vector3 direction)
    {
        _rb.velocity = speed + Time.fixedDeltaTime * direction;
    }

   void KillPlayer(PlayerLifeController playerLife)
    {
        Debug.Log("�El jugador se ha girado 180 grados!");
        playerLife.ModifyLife(100);
    }

   public void ChangePosition(Vector3 newPosition, Quaternion rotation)
   {
        transform.SetPositionAndRotation(newPosition, rotation);
   }

    private void OnTriggerEnter(Collider other)
    {
        KillPlayer(_player.GetComponent<PlayerLifeController>());
    }
    
}





