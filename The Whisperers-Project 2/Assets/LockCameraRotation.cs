using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockCameraRotation : MonoBehaviour
{
    public float rotationYInitial;
    public float rotationZInitial;
    // Update is called once per frame
    void Update()
    {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x,rotationYInitial,rotationZInitial);
    }
}
