using UnityEngine;
using System; 
using UnityEngine.Audio; 

public class AudioManager : MonoBehaviour
{
    public Sound [] sounds;

    public static AudioManager instance; 

    //Este Script puede controlar algunos sonidos del juego pero no el sonido de los personajes

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            DontDestroyOnLoad(gameObject);
            return; 
        }

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.spatialBlend = s.SpatialBlend3D; 
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop; 
        }
    }

    //public void Start()
    //{
    //    Play("Monster"); 
    //}

    public void Play (string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if(s == null)
        {
            Debug.LogWarning("No existeix el Audio: " + name + "!");
            return; 
        }
        s.source.Play(); 
    }

    //s'ha d'utilitzar la seg�ent linia de codi per activar algun s� del Audio Manager : 
    //FindObjectOfType<AudioManager>().Play(nomDelSo_en string); 
}
